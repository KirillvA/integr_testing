var assert = require('assert');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
var geocodingAccuracy = 0.0001;
var serviceUrl = 'https://geocode-maps.yandex.ru/1.x/';

describe('Yandex Map API Test', function() {
  describe('Map Geocode test', function() {

    it('should find object by coord', function(done) {
        function onLoad() {
          var response = JSON.parse(request.responseText);
          var address = response.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.text;
          assert.equal(address, 'Россия, Чувашская Республика, Чебоксары, Московский проспект, 15');
          done();
        };
        var longitude = 47.224291;
        var latitude = 56.145710;
        
        var request = new XMLHttpRequest();
        request.addEventListener('load', onLoad);
        var requestURL = `${serviceUrl}?format=json&geocode=${longitude},${latitude}`;
        request.open('GET', requestURL);
        request.send();
    });

    it('should return coords of object', function(done) {
        function onLoad() {
          var response = JSON.parse(request.responseText);
          var coords = response.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos
          coords = coords.split(' ');
          var correctCoords = [47.224291, 56.145710]
          if (coords.length == 2) {
              assert.ok(Math.abs(Math.abs(coords[0]) - Math.abs(correctCoords[0])) <  geocodingAccuracy && 
              Math.abs(Math.abs(coords[1]) - Math.abs(correctCoords[1])) < geocodingAccuracy);//(coords, '47.224291 56.145710');}
          }
          done();
        };

        var address ="Россия, Чувашская Республика, Чебоксары, Московский проспект, 15";
        var request = new XMLHttpRequest();
        request.addEventListener('load', onLoad);
        var requestURL = `${serviceUrl}?format=json&geocode=${encodeURIComponent(address)}`;
        
        request.open('GET', requestURL);
        request.send();
    });
  });
});
